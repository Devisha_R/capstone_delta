﻿using NLog;
using System.Security.Policy;

namespace CategoryServices.Logger
{
    public class LoggerServices : ILogger
    {

        private static ILogger logger = (ILogger)LogManager.GetCurrentClassLogger();
        public LoggerServices()
        {

        }
        public void LogDebug(string message)
        {
            //logger.Debug(message);
        }



        public void LogError(string message)
        {
            //logger.Error(message);
        }



        public void LogInfo(string message)
        {
            //logger.Info(message);
        }



        public void LogTrace(string message)
        {
            //logger.Warn(message);
        }



        public void LogWarn(string message)
        {
            //logger.Trace(message);
        }
    }
}

