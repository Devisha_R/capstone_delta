﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using OrderServices.DataLayer;
using OrderServices.Handlers;
using OrderServices.Queries;
using Shouldly;
using OrderServices.Models;
using System.Linq;

namespace OrderTest.Queries
{
    public class GetOrderRequestHandler
    {
        private readonly Mock<IOrderService> _mockRepo;



        public GetOrderRequestHandler()
        {
            _mockRepo = Mock.MockRepository.GetOrderService();
        }



        [Fact]
        public async Task GetCategoryListTest()
        {
            var handler = new GetAllOrdersHandlers(_mockRepo.Object);



            var result = await handler.Handle(new GetAllOrdersQuery(), CancellationToken.None);



            result.ShouldBeOfType<List<EcomProducts>>();



            result.Count().ShouldBe(2);
        }
    }
}



