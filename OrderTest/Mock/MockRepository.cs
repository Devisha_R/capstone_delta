﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using OrderServices.DataLayer;
using OrderServices.Models;

namespace OrderTest.Mock
{
    public class MockRepository
    {
        public static Mock<IOrderService> GetOrderService()
        {
            var ordlist = new List<EcomOrders>
            {
                new EcomOrders
                {
                    OrderId =700,
                    ProductId=301, 
                    CustomerId=908,
                    OrderQuantity =90,
                    OrderPrice =8000,
                    ShipmentAddress ="Bhopal"
    }
            };

            var mockrepo = new Mock<IOrderService>();

            mockrepo.Setup(r => r.GetAllOrders()).Returns(ordlist);



            //mockrepo.Setup(r => r.AddCategory(It.IsAny<EcomCategory>().ToString())).Returns((EcomCategory category) =>
            //{
            //    catlist.Add(category);
            //    return catlist.SingleOrDefault(x => x.CategoryId == category.CategoryId);
            //});



            return mockrepo;
        }
    }

}




