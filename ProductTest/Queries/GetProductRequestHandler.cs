﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using ProductServices.DataLayer;
using ProductServices.Models;
using ProductServices.Handlers;
using ProductServices.Queries;
using Shouldly;
using System.Linq;

namespace ProductTest.Queries
{
    public class GetProductRequestHandler
    {
        private readonly Mock<IProductService> _mockRepo;



        public GetProductRequestHandler()
        {
            _mockRepo = Mock.MockRepository.GetProductService();
        }



        [Fact]
        public async Task GetCategoryListTest()
        {
            var handler = new GetAllProductsHandler(_mockRepo.Object);



            var result = await handler.Handle(new GetAllProductsQuery(), CancellationToken.None);



            result.ShouldBeOfType<List<EcomProducts>>();



            result.Count().ShouldBe(1);
        }
    }
}



