﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using ProductServices.DataLayer;
using ProductServices.Models;

namespace ProductTest.Mock
{
    public class MockRepository
    {
        public static Mock<IProductService> GetProductService()
        {
            var prolist = new List<EcomProducts>
            {
                new EcomProducts
                {
                    ProductId =100,
                    CategoryId =301,
                    ProductName ="Laptop",
                    ProductType ="Electronics",
                    ProductPrice =90000,
                    ProductDescription ="electronic product",
                    ProductImg="https://www.bing.com/th?id=OIP.MdCYByzdX9rEVrKPk5MUygHaHa&w=250&h=250&c=8&rs=1&qlt=90&o=6&dpr=1.5&pid=3.1&rm=2"
                }
            };

            var mockrepo = new Mock<IProductService>();

            mockrepo.Setup(r => r.GetAllProducts()).Returns(prolist);



            //mockrepo.Setup(r => r.AddCategory(It.IsAny<EcomCategory>().ToString())).Returns((EcomCategory category) =>
            //{
            //    catlist.Add(category);
            //    return catlist.SingleOrDefault(x => x.CategoryId == category.CategoryId);
            //});



            return mockrepo;
        }
    }

}




