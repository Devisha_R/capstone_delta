﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Text;
using CategoryServices.DataLayer;
using CategoryServices.Handlers;
using CategoryServices.Queries;
using Moq;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using System.Linq;
using Shouldly;
using CategoryServices.Models;

namespace CategoryTestProject.Queries
{
    public class GetCategoryRequestHandler
    {
        private readonly Mock<ICategoryService> _mockRepo;



        public GetCategoryRequestHandler()
        {
            _mockRepo = Mock.MockRepository.GetCategoryService();
        }



        [Fact]
        public async Task GetCategoryListTest()
        {
            var handler = new GetAllCategoryHandler(_mockRepo.Object);



            var result = await handler.Handle(new GetAllCategoryQuery(), CancellationToken.None);



            result.ShouldBeOfType<List<EcomCategory>>();



            result.Count().ShouldBe(2);
        }
    }
}


   