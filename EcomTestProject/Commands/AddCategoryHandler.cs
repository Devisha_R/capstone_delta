﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using CategoryServices.Command;
//using CategoryServices.DataLayer;
//using Moq;
//using System.Threading.Tasks;
//using System.Threading;
//using Xunit;
//using CategoryServices.Models;

//namespace CategoryTestProject.Commands
//{
//    public class AddCategoryHandler
//    {
//        private readonly Mock<ICategoryService> _mockRepo;
//        private readonly EcomCategory _category;

//        public AddCategoryHandler()
//        {
//            _mockRepo = Mocks.MockRepository.GetCategoryService();

//            _category = new EcomCategory
//            {
//                CategoryId = 506,
//                CategoryName = "Stationary"
//            };
//        }

//        [Fact]
//        public async Task AddCategoryTest()
//        {
//            var handler = new AddCategoryHandler(_mockRepo.Object);

//            var result = handler.Handle(new AddCategoryCommand { Category = _category }, CancellationToken.None);

//            var category = await _mockRepo.Object.GetAll();

//            await result.ShouldBeOfType<Task<IEnumerator<EcomCategory>>>();

//            category.Count().ShouldBe(3);
//        }
//    }
//}
